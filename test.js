'use strict'

const path = require('path')
const assert = require('assert')
const relativeUrlToRelativePath = require('.')

// Before
const originalSeparator = path.sep

// Unix
path.sep = '/'
assert.equal(relativeUrlToRelativePath('foo'), './foo', 'Bare (Unix)')
assert.equal(relativeUrlToRelativePath('./foo'), './foo', 'Current (Unix)')
assert.equal(relativeUrlToRelativePath('/foo'), './foo', 'Absolute (Unix)')

// Windows
path.sep = '\\'
assert.equal(relativeUrlToRelativePath('foo'), '.\\foo', 'Bare (Windows)')
assert.equal(relativeUrlToRelativePath('./foo'), '.\\foo', 'Current (Windows)')
assert.equal(relativeUrlToRelativePath('/foo'), '.\\foo', 'Absolute (Windows)')

// After
path.sep = originalSeparator
