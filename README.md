# relative-url-to-relative-path

Convert relative URLs to relative file system paths.

## relativeUrlToRelativePath(url)

`url` is a string containing a relative URL pathname.

Returns a string representing a relative file path.

The file path always starts with `./` to make it relative to the current directory.

Respects the path separator of the host environment, typically `/` or `\`.

For example `./foo.js` or `foo.js` or `/foo` will all be returned as `./foo.js` on macOS and Linux, or `.\foo.js` on Windows.
