'use strict'

const path = require('path')

const currentPath = '.'
const urlSlash = '/'
const urlSlashRegex = new RegExp(urlSlash, 'g')

function relativeUrlToRelativePath (url) {
  const file = url.startsWith(currentPath) ? url
    : url.startsWith(urlSlash) ? currentPath + url
    : currentPath + path.sep + url
  return path.sep === urlSlash ? file : file.replace(urlSlashRegex, path.sep)
}

module.exports = relativeUrlToRelativePath
